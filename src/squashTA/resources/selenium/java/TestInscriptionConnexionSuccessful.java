import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestInscriptionConnexionSuccessful {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
    	///System.setProperty("webdriver.chrome.driver","chromedriver");
    	driver = new HtmlUnitDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @Test
    public void testInscriptionConnexionSuccessful() throws Exception {
        //driver.get("http://localhost/goodle/Source/Vendor/php/register.php");
        driver.get("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test3/preprod/Source/Vendor/php/register.php");
        driver.findElement(By.name("username")).click();
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.name("username")).sendKeys("TestUser3");
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys("TestUser3@mail.com");
        driver.findElement(By.name("password_1")).clear();
        driver.findElement(By.name("password_1")).sendKeys("Azerty123&");
        driver.findElement(By.name("password_2")).clear();
        driver.findElement(By.name("password_2")).sendKeys("Azerty123&");
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys("Alex");
        driver.findElement(By.name("surname")).clear();
        driver.findElement(By.name("surname")).sendKeys("Smith");
        driver.findElement(By.name("bday")).click();
        driver.findElement(By.name("bday")).clear();
        driver.findElement(By.id("bday")).sendKeys("1966-02-09");
        driver.findElement(By.name("reg_user")).click();
        driver.findElement(By.id("identiant")).click();
        driver.findElement(By.id("identiant")).clear();
        driver.findElement(By.id("identiant")).sendKeys("TestUser3");
        driver.findElement(By.id("mdp")).click();
        driver.findElement(By.id("mdp")).clear();
        driver.findElement(By.id("mdp")).sendKeys("Azerty123&");
        driver.findElement(By.id("btnConnexion")).click();

        if((driver.getTitle().equals("Goodle - Connexion"))|| (driver.getTitle().equals("Goodle - Register"))) {
            WebElement element = driver.findElement(By.id("error"));
            verificationErrors.append(element.getText());
            System.out.println(verificationErrors);
        }else{
            assertEquals(driver.getTitle(),"Goodle - Accueil");
        }
    }


    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }


    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
