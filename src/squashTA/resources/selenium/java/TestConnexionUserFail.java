import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestConnexionUserFail {
  private HtmlUnitDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new HtmlUnitDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testConnexionUserFail() throws Exception {
	  //TODO change this link to the website hosted at university, this test will not work on jenkins
    //driver.get("http://localhost/goodle/Source/Vendor/php/connexion.php");
    driver.get("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test3/preprod/Source/Vendor/php/connexion.php");
    driver.findElement(By.id("identiant")).click();
    driver.findElement(By.id("identiant")).clear();
    driver.findElement(By.id("identiant")).sendKeys("Fernando.");
    driver.findElement(By.id("mdp")).click();
    driver.findElement(By.id("mdp")).clear();
    driver.findElement(By.id("mdp")).sendKeys("Azerty123&");
    driver.findElement(By.id("btnConnexion")).click();
    
    if((driver.getTitle().equals("Goodle - Connexion"))) {
      System.out.println(driver.findElement(By.id("error")).getText());
      assertEquals(driver.findElement(By.id("error")).getText(),"User or password invalid" );
	}
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
