import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class TestInscritionUserSizeMinFail {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testInscritionUserSizeMinFail() throws Exception {
        //driver.get("http://localhost/goodle/Source/Vendor/php/register.php");
        driver.get("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test3/preprod/Source/Vendor/php/register.php");
        driver.findElement(By.name("username")).click();
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.name("username")).sendKeys("Us");
        driver.findElement(By.name("email")).click();
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys("usermail@mail.com");
        driver.findElement(By.name("password_1")).click();
        driver.findElement(By.name("password_1")).clear();
        driver.findElement(By.name("password_1")).sendKeys("Azerty123&");
        driver.findElement(By.name("password_2")).clear();
        driver.findElement(By.name("password_2")).sendKeys("Azerty123&");
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys("User1");
        driver.findElement(By.name("surname")).click();
        driver.findElement(By.name("surname")).clear();
        driver.findElement(By.name("surname")).sendKeys("User11");
        driver.findElement(By.id("bday")).click();
        driver.findElement(By.id("bday")).clear();
        driver.findElement(By.id("bday")).sendKeys("1965-12-20");
        driver.findElement(By.name("reg_user")).click();
        if(driver.getTitle().equals("Goodle - Register")) {

            //System.out.println(driver.findElement(By.className("error")).getText());
            assertEquals(driver.findElement(By.id("error")).getText(),"Username must contain at least 3 characters");
        }
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
